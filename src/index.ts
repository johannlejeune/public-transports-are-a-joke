import cron from "node-cron";
import Database from "./Database";
import { TaskGroup, TaskManager } from "./TaskManager";
import { StatsTimeSpan } from "./TaskManager/TweetSNCFStatsTask";
import { ArgvHelper, IArgv } from "./Utils/ArgvHelper";
import { Logger } from "./Utils/Logger";
import { MomentHelper } from "./Utils/MomentHelper";

MomentHelper.setupMoment();

const logger = Logger.setupLogger();

async function main() {
  const argv = ArgvHelper.argv;

  if (argv.cron) {
    scheduleCronTasks();
  }

  if (argv.group) {
    execute(argv);
  }
}

function scheduleCronTasks() {
  // Run daily report every day at 01:04 AM
  cron.schedule("4 1 * * *", async () => {
    return execute({ group: TaskGroup.TASK_GROUP_SNCF, timeSpan: StatsTimeSpan.TIME_SPAN_DAY });
  });
  // Run weekly report every monday at 10:04 AM
  cron.schedule("4 10 * * 1", async () => {
    return execute({ group: TaskGroup.TASK_GROUP_SNCF, timeSpan: StatsTimeSpan.TIME_SPAN_WEEK });
  });
  // Run monthly report every 1st day of each month at 05:04 PM
  cron.schedule("4 17 1 * *", async () => {
    return execute({ group: TaskGroup.TASK_GROUP_SNCF, timeSpan: StatsTimeSpan.TIME_SPAN_MONTH });
  });
  // Run yearly report every 1st day of each year at 07:04 AM
  cron.schedule("4 7 1 1 *", async () => {
    return execute({ group: TaskGroup.TASK_GROUP_SNCF, timeSpan: StatsTimeSpan.TIME_SPAN_YEAR });
  });
}

async function execute(argv: IArgv) {
  const database = new Database();
  try {
    await database.open();

    // Execute tasks if any.
    if (argv.group) {
      await TaskManager.execute(argv.group, argv, database);
    }
  } catch (error) {
    logger.error(error);
  } finally {
    await database.close();
  }
}

main().catch((error) => {
  logger.error(error);
  process.exit(1);
});
