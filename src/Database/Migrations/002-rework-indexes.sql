--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

PRAGMA auto_vacuum = incremental;

DROP INDEX sncf_disruption_ix_severity_effect;
DROP INDEX sncf_disruption_ix_cause;
CREATE INDEX sncf_disruption_ix_day_id_severity_effect ON sncf_disruptions (sncf_disruption_day_id, severity_effect);
CREATE INDEX sncf_disruption_ix_day_id_cause ON sncf_disruptions (sncf_disruption_day_id, cause);

DROP INDEX sncf_disruption_impacted_stop_ix_disruption_id;
DROP INDEX sncf_disruption_impacted_stop_ix_name;
CREATE INDEX sncf_disruption_impacted_stop_ix_day_id_arrival_status ON sncf_disruption_impacted_stops (sncf_disruption_day_id, arrival_status);
CREATE INDEX sncf_disruption_impacted_stop_ix_disruption_id_arrival_status ON sncf_disruption_impacted_stops (sncf_disruption_id, arrival_status);

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP INDEX sncf_disruption_impacted_stop_ix_disruption_id_arrival_status;
DROP INDEX sncf_disruption_impacted_stop_ix_day_id_arrival_status;
CREATE INDEX sncf_disruption_impacted_stop_ix_name ON sncf_disruption_impacted_stops (name);
CREATE INDEX sncf_disruption_impacted_stop_ix_disruption_id ON sncf_disruption_impacted_stops (sncf_disruption_id);

DROP INDEX sncf_disruption_ix_day_id_cause;
DROP INDEX sncf_disruption_ix_day_id_severity_effect;
CREATE INDEX sncf_disruption_ix_cause ON sncf_disruptions (cause);
CREATE INDEX sncf_disruption_ix_severity_effect ON sncf_disruptions (severity_effect);
