import yargs from "yargs";
import { TaskGroup } from "../TaskManager";
import { StatsTimeSpan } from "../TaskManager/TweetSNCFStatsTask";

// Setup CLI arguments.
const argv = yargs
  .option("config", {
    describe: "Specifies the path to the config file.",
    type: "string",
  })
  .option("cron", {
    describe: "Start the CRON scheduler.",
    type: "boolean",
  })
  .option("group", {
    choices: Object.keys(TaskGroup)
      .filter((key) => isNaN(+key))
      .map<string>((name) => (TaskGroup as any)[name]),
    describe: "The task group to execute.",
    type: "string",
  })
  .option("time-span", {
    choices: Object.keys(StatsTimeSpan)
      .filter((key) => isNaN(+key))
      .map<string>((name) => (StatsTimeSpan as any)[name]),
    describe: "The time span for which to process the stats (only SNCF stats).",
    type: "string",
  })
  .option("custom-time-span", {
    describe: "A custom time span for which to process the stats (only SNCF stats).",
    type: "string",
  })
  .help("help")
  .parseSync();

export interface IArgv {
  config?: string;
  cron?: boolean;
  customTimeSpan?: string;
  group?: TaskGroup;
  timeSpan?: StatsTimeSpan;
}

export class ArgvHelper {
  public static get argv(): IArgv {
    return {
      config: argv.config,
      cron: argv.cron,
      group: argv.group as TaskGroup,
      timeSpan: argv.timeSpan as StatsTimeSpan,
      customTimeSpan: argv.customTimeSpan,
    };
  }
}
