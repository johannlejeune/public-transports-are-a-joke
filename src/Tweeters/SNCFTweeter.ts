import { TweetV2PostTweetResult, TwitterApi, TwitterApiReadWrite } from "twitter-api-v2";
import SNCFDisruptionDaysAggregate from "../Database/Model/SNCFDisruptionDaysAggregate";
import { SNCFFormatter } from "../Formatters/SNCFFormatter";
import { Constants } from "../Utils/Constants";
import { Logger } from "../Utils/Logger";

const logger = Logger.getLogger();

export class SNCFTweeter {
  private readonly _formatter: SNCFFormatter;
  private readonly _data: SNCFDisruptionDaysAggregate;
  private readonly _client: TwitterApiReadWrite;

  constructor(formatter: SNCFFormatter, data: SNCFDisruptionDaysAggregate) {
    if (
      Constants.TWITTER_ACCESS_TOKEN === undefined ||
      Constants.TWITTER_ACCESS_TOKEN_SECRET === undefined ||
      Constants.TWITTER_CONSUMER_KEY === undefined ||
      Constants.TWITTER_CONSUMER_SECRET === undefined
    ) {
      throw new Error("No Twitter tokens given.");
    }

    this._formatter = formatter;
    this._data = data;
    this._client = new TwitterApi({
      appKey: Constants.TWITTER_CONSUMER_KEY,
      appSecret: Constants.TWITTER_CONSUMER_SECRET,
      accessToken: Constants.TWITTER_ACCESS_TOKEN,
      accessSecret: Constants.TWITTER_ACCESS_TOKEN_SECRET,
    }).readWrite;
  }

  public async tweet(): Promise<void> {
    logger.info("Posting thread...");

    await this.postThread([
      this._formatter.formatDisruptions(this._data),
      this._formatter.formatDisruptionCauses(this._data.causes),
      this._formatter.formatDisruptionStops(this._data.impactedStops),
    ]);
  }

  private async postThread(statuses: string[]): Promise<TweetV2PostTweetResult[]> {
    // Check if any of the Tweets will exceed the maximum Tweet length, just in case.
    if (statuses.some((status) => status.length > Constants.TWITTER_MAX_TWEET_LENGTH)) {
      throw new Error(
        `Some Tweets from the thread exceed the maximum Tweet length (${Constants.TWITTER_MAX_TWEET_LENGTH}).`
      );
    }

    return this._client.v2.tweetThread(statuses);
  }
}
